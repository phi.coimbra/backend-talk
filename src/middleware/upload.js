module.exports = () => {
    return async context => {
        //console.log(context)
        if (!context.data.uri && context.params.file){
            const file = context.params.file;
            const uri = dauria.getBase64DataURI(file.buffer, file.mimetype);
            context.data = {uri: uri};
           // context.resto = 'context.data.email';
            //console.log(context)
        }
        context.service.emit('myEvent', { status: 'completed' });
  
      return context;
    }
  }
