const users = require('./users/users.service.js');
const uploads = require('./uploads/uploads.service.js');
const pouchserver = require('./pouchserver/pouchserver.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(uploads);
 // const pouchserver = await require('./pouchserver/pouchserver.service.js');
  app.configure(pouchserver);
};
