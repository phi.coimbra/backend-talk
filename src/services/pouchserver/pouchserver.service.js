// Initializes the `pouchserver` service on path `/pouchserver`
const { Pouchserver } = require('./pouchserver.class');
const hooks = require('./pouchserver.hooks');
const {createRxDatabase} = require("rxdb/plugins/core")
const {addRxPlugin} = require("rxdb/plugins/core")
const { RxDBEncryptionPlugin } = require('rxdb/plugins/encryption');
const { RxDBReplicationPlugin }  = require('rxdb/plugins/replication');
const { RxDBLeaderElectionPlugin }  = require('rxdb/plugins/leader-election');
const { RxDBQueryBuilderPlugin }  = require('rxdb/plugins/query-builder');
const { RxDBValidatePlugin }  = require('rxdb/plugins/validate');
const _ = require('lodash');

addRxPlugin(RxDBValidatePlugin);
addRxPlugin(RxDBQueryBuilderPlugin);
addRxPlugin(RxDBLeaderElectionPlugin);
addRxPlugin(RxDBReplicationPlugin);
addRxPlugin(RxDBEncryptionPlugin);

const { RxDBServerPlugin }  = require('rxdb/plugins/server');
addRxPlugin(RxDBServerPlugin);
const  MemoryAdapter  = require('pouchdb-adapter-memory');
addRxPlugin(MemoryAdapter);

addRxPlugin(require('pouchdb-adapter-node-websql'));
addRxPlugin(require('pouchdb-adapter-http'));

//const {userSchema, eventoSchema, imageSchema, quizSchema} = require('./schema/Schema.js');

const imageSchema = {
  version: 0,
  title: 'imagem schema',
  description: 'imagens dos perfis',
  type: 'object',
  properties: {
      email: {
          type: 'string',
      },
      id: {
          type: 'string',
          primary: true,
      },
      col:{
          type: 'string',
          default:'images'
      },
  },
}

const eventoSchema = {
  version: 0,
  title: 'evento schema',
  description: 'nomes de eventos',
  type: 'object',
  properties: {
      nome: {
          type: 'string',
      },
      id: {
          type: 'string',
          primary: true,
      },
      col:{
          type: 'string',
          default:'evento'
      },
  }
}


const userSchema = {
  version: 0,
  title: 'user schema',
  description: 'usuarios e cadastro de informações',
  type: 'object',
  properties: {
      id: {
          type: 'string',
          primary: true,
      },
      nome: {
          type: 'string',
      },
      sobrenome: {
          type: 'string',
      },
      email: {
          type: 'string',
      },
      logged: {
          type: 'boolean',
          default:false
      },
      evento:{
          type: 'string',
          default:''
      },
      device:{
          type: 'string',
          default:''
      },
      col:{
          type: 'string',
          default:'users'
      },
      image: {
          type: 'string',
          default:'http://192.168.1.48:3030/images/avatar.png'
      },
  },
  required: ['nome', 'sobrenome', 'email'],
};

const quizSchema = {
  version: 0,
  title: 'schema de quiz',
  type: 'object',
  description: 'quiz para o evento',
  properties: {
      id: {
          type: 'string',
          primary: true,
      },
      info: {
          type: 'string'
      },
      evento: {
          type: 'string'
      },
      perguntas: {
          type: 'array',
          items: {
              type: 'object',
              properties: {
                  titulo: {
                      type: 'string'
                  },
                  alternativas: {
                      type: 'array',
                      items: {
                          type: 'object',
                          properties: {
                              titulo: {
                                  type: 'string'
                              }
                          }
                      }       
                  },
                  resposta: {
                      type: 'string'
                  }
              }
          }       
      }
  }
};

const Database = {};

const dbName = 'antidoto';
const syncURL = 'http://192.168.1.48:3000/';

const myDB = {}

const create = async () => {
   
    console.log('database')

    
    const db = await createRxDatabase({
        name: dbName,
        adapter: 'websql',
        password: 'antidoto1234',
        multiInstance: false,
    });

    await db.collection({
        name: 'users',
        schema:userSchema,
    });
  
    console.log('users')
  
    await db.collection({
      name: 'evento',
      schema:eventoSchema,
    });
  
    console.log('evento')
  
    await db.collection({
      name: 'images',
      schema:imageSchema,
    });
  
    console.log('images')
  
    await db.collection({
      name: 'quiz',
      schema:quizSchema,
     
    });
  
    db.users.sync({
        remote: syncURL + dbName + '/users/',
        options: {
            live: true,
            retry: true,
        },
    });
  
    db.evento.sync({
      remote: syncURL + dbName + '/evento/',
      options: {
          live: true,
          retry: true,
      },
    });
  
    db.images.sync({
      remote: syncURL + dbName + '/images/',
      options: {
          live: true,
          retry: true,
      },
    });
  
    const quizSync = db.quiz.sync({
      remote: syncURL + dbName + '/quiz/',
      options: {
          live: true,
          retry: true,
      },
    });
  
    db['quiz'].findOne().where('evento').eq('antidoto').exec().then(doc => {
        if(!doc){
          console.log('TEM QUIZ?', doc)
          console.log('CRIA QUIZ')
          createQuiz(db)
        }else{
          console.log('TEM QUIZ?', doc.perguntas)
        }
    })

    db['evento'].findOne().where('nome').eq('antidoto').exec().then(doc => {
      if(!doc){
        console.log('TEM EVENTO?', doc)
        console.log('CRIA EVENTO')
        createEvento(db)
      }else{
        console.log('TEM EVENTO?', doc.nome)
      }
    })
      
    const {app, server} = db.server({
        path: '/antidoto', // omitted when startServer is false and force set to /
        port: 5050,  // omitted when startServer is false
        cors: false,  // disable CORS-headers (default) - you probably want to configure CORS in your main app
        startServer: true // do not start express server
    });

    return {db:db, server:app};
};

let createPromise = null;
Database.get = async () => {
  console.log('start database pouch')
    if (!createPromise) createPromise = create();
    return createPromise.db;
};

Database.get()


module.exports = function (app) {
  const options = {
    paginate: app.get('paginate')
  };

  //if (!createPromise) createPromise = await create();

  //console.log('DATABASE: ')

  // Initialize our service with any options it requires
  app.use('/pouchserver', /*createPromise.server*/ new Pouchserver(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('pouchserver');

  service.hooks(hooks);
}

function createEvento(db){
  var randomNumber = _.times(4, () => _.times(8, () => _.random(35).toString(36)).join('')).join('-');
  const doc = db.evento.insert({nome:'antidoto', id:randomNumber});
}

function createQuiz(db){
  var randomNumber = _.times(4, () => _.times(8, () => _.random(35).toString(36)).join('')).join('-');

  const docquiz = {
    id:randomNumber,
    info:'quiz primeiro',
    evento:'antidoto',
    perguntas:[{
      titulo:'O que é o Antídoto Talk?',
      alternativas:[{
        titulo:'Uma plataforma de comunicação',
      },{
        titulo:'Aplicativo para realizar lives e apresentar videoaulas',
      },{
        titulo:'Portal de informações',
      },{
        titulo:'Plataforma para realização de eventos, congressos e treinamento virtuais e off-line',
      },{
        titulo:'Todas as alternativas anteriores estão corretas',
      }],
      resposta:'4',
    },
    {
      titulo:'Quais as principais funções da plataforma?',
      alternativas:[{
        titulo:'Basicamente organizar um cadastro se usuários',
      },{
        titulo:'Apenas enviar alertas via push',
      },{
        titulo:'Criar uma rede social com foco em relacionamentos pessoais',
      },{
        titulo:'Comunicar, organizar, interagir, entreter e treinar',
      },{
        titulo:'Apenas a alternativa "d" está correta.',
      }],
      resposta:'4',
    },
    {
      titulo:'Na ferramenta de live e videoaulas é possível...',
      alternativas:[{
        titulo:'Assistir o conteúdo só na data e hora pré marcada',
      },{
        titulo:'Assistir o conteúdo a hora que quiser',
      },{
        titulo:'Realizar perguntas via chat em tempo real',
      },{
        titulo:'Criar uma rede de conexão entre empresa, cliente e colaboradores',
      },{
        titulo:'Todas as alternativas anteriores estão corretas',
      }],
      resposta:'4',
    },
    {
      titulo:'O Antídoto Talk é uma ótima opção para:',
      alternativas:[{
        titulo:'Passar o tempo',
      },{
        titulo:'Realizar eventos, congressos, treinamentos virtuais e off-line',
      },{
        titulo:'Criar uma rede de comunicação entre empresa e pessoas',
      },{
        titulo:'Comunicar e treinar em tempos de Covid-19',
      },{
        titulo:'Apenas a alternativa "a" está errada',
      }],
      resposta:'4',
    }]
  }

  db.quiz.insert(docquiz)

}

/*
mainApp.use('/db', app);
mainApp.use('/', (req, res) => res.send('hello'));
mainApp.listen(3000, () => console.log(`Server listening on port 3000`));
*/
