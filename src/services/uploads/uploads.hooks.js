const setUpload = require('../../middleware/upload');

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [setUpload()],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [(e) => console.log('POSTED:', e.result.id)],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
