// Initializes the `uploads` service on path `/uploads`
const multer = require('multer');
const multipartMiddleware = multer();
const { Uploads } = require('./uploads.class');
const hooks = require('./uploads.hooks');

const blobService = require('feathers-blob');
// Here we initialize a FileSystem storage,
// but you can use feathers-blob with any other
// storage service like AWS or Google Drive.
const fs = require('fs-blob-store');

const blobStorage = fs('./uploads');

module.exports = function (app) {
  const options = {
    paginate: app.get('paginate'),
    events: ['myEvent'],
  };

  // Initialize our service with any options it requires
  /*app.use('/uploads', 
    //new Uploads(options, app),
    blobService({ Model: blobStorage})
  
  );*/
  app.use('/uploads',
    // multer parses the file named 'uri'.
    // Without extra params the data is
    // temporarely kept in memory
    multipartMiddleware.single('uri'),
    // another middleware, this time to
    // transfer the received file to feathers
    function(req,res,next){
        req.feathers.file = req.file;
        next();
    },
    blobService({Model: blobStorage, events: ['myEvent']})
);

  //app.use('/uploads', blobService({ Model: blobStorage}))

  // Get our initialized service so that we can register hooks
  const service = app.service('uploads');

  service.hooks(hooks);
};
