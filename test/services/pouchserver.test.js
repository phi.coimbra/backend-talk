const assert = require('assert');
const app = require('../../src/app');

describe('\'pouchserver\' service', () => {
  it('registered the service', () => {
    const service = app.service('pouchserver');

    assert.ok(service, 'Registered the service');
  });
});
